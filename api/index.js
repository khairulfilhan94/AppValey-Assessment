var sample_data = require('../data/sample.json');
var sample_category = require('../data/category.json');


export const fetchData = (category,level) => {
    const filtered = sample_data.filter(d => d.category === category).filter(d => d.level === level);
    return filtered;
};

export const fetchCategory = category => {
    return sample_category;
};