import React from 'react';
import { ScrollView,TouchableOpacity, Text, View, StyleSheet, Image } from 'react-native';
import Icons from '../../constants/Icons';
import { Icon } from 'expo';

export default class DashboardScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    const params = navigation.state.params || {};
    return {
      title: 'Dashboard',
      headerTitleStyle: {
        color: '#fff'
      },
      headerLeft: (
        <TouchableOpacity>
          <Icon.MaterialIcons
            name='menu'
            size={26}
            style={{ color: '#fff', marginLeft: 10 }}
          />
        </TouchableOpacity>
      ),
      headerStyle: {
        backgroundColor: '#0288D1'
      },
    }
  }
  render() {
    return (
      <ScrollView style={styles.container}>
        <View style={styles.child}>
          <View style={styles.image_container}>
            <Image source={{ uri: Icons.development }} style={styles.images} />
          </View>
          <Text style={styles.text}>Development in progress ...</Text>
          <Text style={styles.text}>For Assessment Only ...</Text>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
  child: {
    marginTop : 20,
    justifyContent: 'center', 
alignItems: 'center' 

  },
  text: {
    textAlign: 'center'
  },
  image_container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  images: {
    width: 100,
    height: 100
  }
});
