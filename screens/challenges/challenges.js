import React from 'react';
import { Text, TouchableOpacity, View, ActivityIndicator, TextInput, StyleSheet, FlatList } from 'react-native';
import { SearchBar } from 'react-native-elements';
import { Icon } from 'expo';
import { Picker } from 'native-base';
import StepIndicator from 'react-native-step-indicator';
import { sample_data } from '../../data';
import { fetchData } from '../../api';
import Card from '../../components/card';
import Modal from '../../components/modal';

const labels = ["Beginner", "Intermediate", "Advanced"];
const customStyles = {
  stepIndicatorSize: 25,
  currentStepIndicatorSize: 30,
  separatorStrokeWidth: 2,
  currentStepStrokeWidth: 3,
  stepStrokeCurrentColor: '#0288D1',
  stepStrokeWidth: 3,
  stepStrokeFinishedColor: '#0288D1',
  stepStrokeUnFinishedColor: '#aaaaaa',
  separatorFinishedColor: '#0288D1',
  separatorUnFinishedColor: '#aaaaaa',
  stepIndicatorFinishedColor: '#0288D1',
  stepIndicatorUnFinishedColor: '#ffffff',
  stepIndicatorCurrentColor: '#ffffff',
  stepIndicatorLabelFontSize: 13,
  currentStepIndicatorLabelFontSize: 13,
  stepIndicatorLabelCurrentColor: '#0288D1',
  stepIndicatorLabelFinishedColor: '#ffffff',
  stepIndicatorLabelUnFinishedColor: '#aaaaaa',
  labelColor: '#999999',
  labelSize: 13,
  currentStepLabelColor: '#0288D1'
}


export default class ChallengesScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    const params = navigation.state.params || {};
    return {
      title: (
        <TouchableOpacity onPress={params.showModal} >
          <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ color: '#fff' }}>{params.active} Challenges</Text>
            <Icon.Ionicons
              name='ios-arrow-down-outline'
              size={20}
              style={{ color: '#fff', marginLeft: 10 }}
            />
          </View>
        </TouchableOpacity>
      ),
      headerTitleStyle: {
        color: '#fff'
      },
      headerLeft: (
        <TouchableOpacity>
          <Icon.MaterialIcons
            name='menu'
            size={26}
            style={{ color: '#fff', marginLeft: 10 }}
          />
        </TouchableOpacity>
      ),
      headerStyle: {
        backgroundColor: '#0288D1'
      },
    }
  }

  constructor(props) {
    super(props);
    // this.arrayholder = [];
  }


  state = {
    currentPosition: 0,
    loading: true,
    active: 'Cardio',
    level: 'Beginner',
    sample_data: [],
    arrayholder :[],
    showModal: false
  }

  componentDidMount = async () => {
    this.props.navigation.setParams({ active: this.state.active, showModal: this.showModal });
    let sample_data = await fetchData(this.state.active, this.state.level);
    this.setState({
      loading: false
    })
    console.log(sample_data);
    this.setState({
      sample_data,
      arrayholder:  sample_data
    });
  }

  showModal = () => {
    this.setState({
      showModal: true
    })
  }

  onModalClose = async active => {
    let sample_data = await fetchData(active, this.state.level);
    this.props.navigation.setParams({ active });
    this.setState({
      loading: false,
      sample_data,
      active,
      showModal: false
    })
  }

  _keyExtractor = (item, index) => item.id;

  _onPressItem = val => {
  };

  _renderItem = ({ item }) => (
    <Card item={item} />
  );

  handleLevel = async val => {
    let level = '';
    this.setState({
      currentPosition: val
    })
    if (val === 0) {
      level = 'Beginner'
      // this.setState({
      //   level: 'Beginner'
      // })
    } else if (val === 1) {
      level = 'Intermediate'
      // this.setState({
      //   level: 'Intermediate'
      // })
    } else {
      level = 'Advanced'
      // this.setState({
      //   level: 'Advanced'
      // })
    }
    let sample_data = await fetchData(this.state.active, level);
    this.setState({
      sample_data,
    })
  }

  onSearch = param => {
    const arrayholder = this.state.arrayholder.filter(d => d.title.indexOf(param) > -1);
    this.setState({
      sample_data: arrayholder
    })
    console.log(arrayholder)
  }


  render() {
    const { loading } = this.state;
    if (loading) return <ActivityIndicator size="small" color="#000" />;
    return (
      <View style={styles.container}>
        <SearchBar lightTheme placeholder="Search for a Challenge" onChangeText={this.onSearch} />
        <View style={{ height: 20 }} />
        <StepIndicator
          onPress={this.handleLevel}
          stepCount={3}
          customStyles={customStyles}
          currentPosition={this.state.currentPosition}
          labels={labels}
        />
        {this.state.sample_data.length === 0 && <Text style={{ textAlign: 'center', marginTop: 20 }}>No data </Text>}
        <FlatList
          contentContainerStyle={{ padding: '2%' }}
          data={this.state.sample_data}
          extraData={this.state}
          keyExtractor={this._keyExtractor}
          renderItem={this._renderItem}
        />
        <Modal isVisible={this.state.showModal} onModalClose={this.onModalClose} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
  search_container: {
    flex: 0.1,
    backgroundColor: 'whitesmoke',
    alignContent: 'space-between',
    flexDirection: 'row'
  },
  child: {
    marginTop: 20,
    justifyContent: 'center',
    alignItems: 'center'

  },
  text: {
    textAlign: 'center'
  },
  image_container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  images: {
    width: 100,
    height: 100
  }
});
