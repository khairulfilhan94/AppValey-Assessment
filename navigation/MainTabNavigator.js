import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';
import { Icon } from 'expo';

import TabBarIcon from '../components/TabBarIcon';
import DashboardScreen from '../screens/dashboard/dashboard';
import TimelineScreen from '../screens/timeline/timeline';
import NearbyScreen from '../screens/nearby/nearby';
import ChallengesScreen from '../screens/challenges/challenges';

const DashboardStack = createStackNavigator({
  Dashboard: DashboardScreen,
});

DashboardStack.navigationOptions = {
  tabBarLabel: 'Dashboard',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name='dashboard'
    />
  ),
  tabBarOptions: {
    activeTintColor: '#E91E63',
  },
};

const TimelineStack = createStackNavigator({
  Timeline: TimelineScreen,
});

TimelineStack.navigationOptions = {
  tabBarLabel: 'Timeline',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name='list'
    />
  ),
  tabBarOptions: {
    activeTintColor: '#E91E63',
  },
};

const NearbyStack = createStackNavigator({
  Nearby: NearbyScreen,
});

NearbyStack.navigationOptions = {
  tabBarLabel: 'Nearby',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name='map-marker'
    />
  ),
  tabBarOptions: {
    activeTintColor: '#E91E63',
  },
};

const ChallengeStack = createStackNavigator({
  Challenges: ChallengesScreen,
});

ChallengeStack.navigationOptions = {
  tabBarLabel: 'Challenges',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name='trophy'
    />
  ),
  tabBarOptions: {
    activeTintColor: '#E91E63',
  },
};


export default createBottomTabNavigator({
  DashboardStack,
  TimelineStack,
  NearbyStack,
  ChallengeStack,
});
