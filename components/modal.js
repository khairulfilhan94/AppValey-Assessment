import React, { PureComponent } from 'react';
import { StyleSheet, TouchableOpacity, View, Text, Image, ScrollView } from 'react-native';
import PropTypes from 'prop-types';
import Modal from "react-native-modal";
import { fetchCategory } from '../api/';
import { ListItem } from 'native-base';

const styles = StyleSheet.create({

});

export default class ModalComponent extends PureComponent {

    static defaultProps = {
    };

    state = {
        data: []
    };

    componentDidMount = async () => {
        let data = await fetchCategory();
        this.setState({
            data
        })
    }

    render() {
        return (
            <View>
                <Modal isVisible={this.props.isVisible}>
                    <ScrollView style={{ flex: 1, backgroundColor: '#fff' }}>
                        <Text style={{ textAlign: 'center', marginTop: 10 }}>Select Category</Text>
                        {this.state.data.map((data, i) => {
                            return <ListItem onPress={() => this.props.onModalClose(data.title)} >
                                <Text>{data.title}</Text>
                            </ListItem>
                        })}
                    </ScrollView>
                </Modal>
            </View>
        );
    }
}
