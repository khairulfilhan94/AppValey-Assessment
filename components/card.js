import React, { PureComponent } from 'react';
import { StyleSheet, Image, View } from 'react-native';
import PropTypes from 'prop-types';
import { Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right } from 'native-base';

const styles = StyleSheet.create({
    card_container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between'

    },
    item: {
        flex: 1,
        flexDirection: 'row',
    }
});

export default class CardComponent extends PureComponent {

    static defaultProps = {
        rounded: true,
        loading: false,
        disabled: false,
    };

    state = {};
    render() {
        const { item } = this.props;
        console.log(item)
        return (
            <Card>
                <CardItem cardBody>
                    <Image source={{ uri: item.img }} style={{ height: 150, width: null, flex: 1 }} />
                </CardItem>
                <CardItem>
                    <Text style={{ fontSize: 20 }}>{item.title}</Text>
                </CardItem>
                <View style={styles.card_container}>
                    <View style={styles.item}>
                        <Icon style={{ color: '#FFA000', fontSize: 13, marginLeft: 10 }} name="thumbs-up" />
                        <Text style={{ color: '#FFA000', fontSize: 13, marginLeft: 10 }}>{item.level}</Text>
                </View>
                <View style={styles.item}>
                        <Icon style={{ color: '#263238', fontSize: 13 }} name="thumbs-up" />
                        <Text style={{ color: '#263238', fontSize: 12, marginLeft: 10 }}>{item.calories} calories</Text>
                    </View>
                    <View style={styles.item}>
                        <Text style={{ color: '#1565C0', fontSize: 13, marginLeft: 10 }}>+{item.point} Points</Text>
                    </View>
                </View>
                <View style={{ height: 10 }} />
            </Card >
        );
    }
}
